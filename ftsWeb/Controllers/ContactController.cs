﻿using Data.Models;
using Data.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ftsWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private readonly ContactService _contatcServices;

        public ContactController(ContactService contactServices)
        {
            _contatcServices = contactServices;
        }

        [HttpGet]
        //public async Task<List<ContactModel>> List()
        //{
        //    return await _contatcServices.GetAsync();
        //}

        [HttpPost]
        public async Task<ContactModel> Post(ContactModel produto)
        {
            await _contatcServices.CreateAsync(produto);

            return produto;
        }
    }
}
