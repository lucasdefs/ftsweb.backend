﻿using Data.Models;
using Data.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FtsWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly PersonServices _personServices;
        private readonly ContactService _contatcServices;

        public PersonController(PersonServices personServices, ContactService contatcServices)
        {
            _personServices = personServices;
            _contatcServices = contatcServices;
        }

        [HttpGet]
        public async Task<List<PersonModel>> List()
        {
            return await _personServices.GetAsync();
        }

        [HttpPost]
        public async Task<PersonModel> Post(PersonModel produto)
        {
            await _personServices.CreateAsync(produto);

            return produto;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PersonModel>> Get(string id)
        {
            PersonModel produto = await _personServices.GetByIdAsync(id);

            var contact = await _contatcServices.GetAsync(id);

            produto.Contact = contact;

            if (produto == null)
            {
                return NotFound(); // Retorna 404 Not Found se o produto não for encontrado
            }

            return produto;
        }

        [HttpPut]
        public async Task<ActionResult<PersonModel>> Update(string id, PersonModel produto)
        {
            var existingProduto = await _personServices.GetByIdAsync(id);

            if (existingProduto == null)
            {
                return NotFound(); // Retorna 404 Not Found se o produto não for encontrado
            }

            // Atualiza as propriedades do produto existente com as do produto recebido
            existingProduto.Name = produto.Name;
            existingProduto.Phone = produto.Phone;
            existingProduto.Document = produto.Document;

            await _personServices.UpdateAsync(id, existingProduto);

            return existingProduto;
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(string id)
        {
            var existingProduto = await _personServices.GetByIdAsync(id);

            if (existingProduto == null)
            {
                return NotFound(); // Retorna 404 Not Found se o produto não for encontrado
            }

            await _personServices.DeleteAsync(id);

            return NoContent(); // Retorna 204 No Content indicando sucesso sem conteúdo
        }
    }
}
