﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Models.Enuns;

namespace Data.Models
{
    public class ContactModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }
        [BsonElement("Phone")]
        public string Phone { get; set; }
        [BsonElement("Type")]
        public TypePhone Type { get; set; }
        [BsonElement("Phone2")]
        public string? Phone2 { get; set; }
        [BsonElement("Type2")]
        public TypePhone? Type2 { get; set; }
        [BsonElement("PersonId")]
        public string? PersonId { get; set; }

        //public ContactModel(string phone, string phone2, TypePhone t1, TypePhone t2, string personId)
        //{
        //    //this.Phone = phone;
        //    //this.Phone2 = phone2;
        //    //this.Type = t1;
        //    //this.Type2 = t2;
        //    //this.PersonId = personId;
        //}

        public TypePhone ChangeType(TypePhone status)
        {
            switch (status)
            {
                case TypePhone.cel:
                    status = TypePhone.cel;
                    break;
                case TypePhone.tel:
                    status = TypePhone.tel;
                    break;
            }
            return status;
        }
    }
}
