﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class PersonModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }
        [BsonElement("Name")]
        public string Name { get; set; }
        [BsonElement("Document")]
        public string Document { get; set; }
        [BsonElement("Phone")]
        public string Phone { get; set; }
        [BsonElement("Country")]
        public string Country { get; set; }
        [BsonElement("State")]
        public string State { get; set; }
        public IEnumerable<ContactModel> Contact { get; set; }
    }
}
