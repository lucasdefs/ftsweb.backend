﻿using Data.DbContext;
using Data.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Services
{
    public class ContactService
    {
        readonly IMongoCollection<ContactModel> _contactRepository;

        public ContactService(IOptions<MongoDBContext> mongoService)
        {
            var mongoClient = new MongoClient(mongoService.Value.ConnectionString);
            var mongoDatabase = mongoClient.GetDatabase(mongoService.Value.DatabaseName);

            _contactRepository = mongoDatabase.GetCollection<ContactModel>("Contacts");
        }

        public async Task CreateAsync(ContactModel contact)
        {
            await _contactRepository.InsertOneAsync(contact);
        }

        public async Task DeleteAsync(string id)
        {
            await _contactRepository.DeleteOneAsync(x => x.Id == id);
        }

        public async Task<List<ContactModel>> GetAsync(string id) =>
            await _contactRepository.Find(x => x.PersonId == id).ToListAsync();


        public async Task<ContactModel> GetByIdAsync(string id) =>
            await _contactRepository.Find(x => x.Id == id).FirstOrDefaultAsync();

        public async Task UpdateAsync(string id, ContactModel contact)
        {
            await _contactRepository.ReplaceOneAsync(x => x.Id == id, contact);
        }
    }
}
